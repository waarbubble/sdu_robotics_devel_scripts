# SDU_Robotics_devel_scripts

This is a repository to keep track of scripts used to speed up development for sdurobotics

## RobWork build and setup

### robwork_depend
Download all RobWork dependencies on ubuntu with:
```bash
robwork_depend
```

### RobWork Build scripts
The main commands to use when building robwork is:
- robwork_build
- robwork_cmake
- robwork_clean

all three commands take one or more targets as arguments. The list of targets are as follows:
- RobWork target names:
  - robwork
  - rw
  - RobWork
- RobWork examples "RobWork/example"
  - rw-ex-top
- RobWork cpp example "RobWork/example/cpp"
  - rw-ex-cpp
- RobWorkStudio target names:
  - robworkstudio
  - rws
  - RobWorkStudio
- RobWorkStudio cpp example "RobWorkStudio/example/cpp"
  - rws-ex-cpp
- RobWorkSim target names:
  - robworksim
  - rwsim
  - RobWorkSim
- RobWorkSim examples "RobWorkSim/example"
  - rwsim-ex-top
- RobWorkHardware target names:
  - robworkhardware
  - rwhw
  - RobWorkHardware
- Toplevel RobWork camkefile building all 4 projects at once
  - rw-top
- RobWork documentation needs all 4 projects to be build
  - rw-doc

The targets can be chained, so that the order of the targets are the order they are processed in. 
If an error happens during processing the following targets are immediately cancelled.
```bash
robwork_cmake rw rwhw rws rwsim
robwork_build rw rw-ex-top rws rwhw rwsim rw-doc
robwork_clean rws rw rw-ex-top
```

### robwork_build
This command first checks if cmake has been configured if not it will run robwork_cmake $target before building the target.

other then the standard targets robwork build also understands "deb", which will build a debian pkg provided that the debian-pkg branch with the debian folder is available from the git.

### robwork_cmake 
This command can be used to run cmake on robwork targets without building

### robwork_clean 
This command cleans up the robwork directory so that you can make a clean build

The following extra targets are available:
- Clean all git ignored files in the whole repository
  - all
  - rw_all
- Clean Debian build
  - deb

### robwork_find_dir
This command returns the robwork directory.
The command checks if you are currently in a subdirectory of robwork and returns the path to Top RobWork folder. If the path isn't found this way it will check if RobWork_DIR is defined in ~/.srds_settings and use that.

```bash
robwork_find_dir && echo "found dir" || echo "didn't find dir"
DIR=$(robwork_find_dir)
```

### robwork_setup
This is a terminal GUI tool that can be used to setup RobWork. It takes the user through the setup steps of getting the dependency's and downloading robwork. Right now it requires sudo access to use.

## RobWork git management
These commands purpose is to make it easier to du manage the git aspect of robwork.

### git_clean_branches
This command cleans all git branches that has been deleted on remote

### git_rw_create_branch
This creates a new branch from master after updating master to be up to date with upstream/devel.
The command requires that upstream has been configured.

This can be done with either:
git_rw_setup_https_upstream or git_rw_setup_ssh_upstream

```bash
git_rw_create_branch name_of_new_branch
```

### git_rw_master_update
this updates the master with the newest updates from upstream/devel


### git_rw_setup_https_upstream
setup upstream branch with https

### git_rw_setup_ssh_upstream
setup upstream branch with ssh. this can only be used if you have uploaded a ssh certificate to you account.

## Other
Other scripts
### srds_settings
This command can be used to get or set the settings used for everything in this project.
the settings are stored in ~/.srds_settings

this command supports changing the default cmake arguments for robwork with the following arguments:
- SWIG=[ON|OFF]
- JAVA=[ON|OFF]
- PYTHON=[ON|OFF]
- LUA=[ON|OFF]
- <package_name>=[ON|OFF]

example
```bash
srds_settings SWIG=OFF PYTHON=ON sdurws_log=OFF
```

### ssh_sdur

This can be used to ssh into SDU robotics machines, if you are on the correct network.

```bash
#command used where number is the pc number you wanna access and name is your user name
ssh_sdur pc=number user=name
```

